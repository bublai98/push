package com.example.securecrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.securecrud.model.user;

public interface UserRepository extends JpaRepository<user, Integer> {

}
