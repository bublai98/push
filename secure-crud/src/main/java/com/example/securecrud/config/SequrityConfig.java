package com.example.securecrud.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class SequrityConfig {
	
	protected void configure(HttpSecurity http) throws Exception{
		http.csrf().disable();
	}
	@Bean
	public  BCryptPasswordEncoder encodePWD() { 
		return new org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder();
	}
}
