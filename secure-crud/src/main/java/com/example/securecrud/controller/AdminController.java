package com.example.securecrud.controller;

import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.securecrud.repository.UserRepository;

@RestController
public class AdminController {

	@Autowired
	private UserRepository userRepository;
	private BCryptPasswordEncoder passwordEncoder;
	@PostMapping("/admin/add")
	public String addUserByAdmin(@RequestBody com.example.securecrud.model.user user1) {
		String pwd=user1.getPassword();
		String encryptPass=passwordEncoder.encode(pwd);
		user1.setPassword(encryptPass);
		userRepository.save(user1);
		return "user added successfully!!";
	}
}
