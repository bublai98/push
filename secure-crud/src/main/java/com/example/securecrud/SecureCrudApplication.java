package com.example.securecrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecureCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecureCrudApplication.class, args);
	}

}
